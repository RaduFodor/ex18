package ro.orange;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromFile {

    public static void main(String[] args) {
        // get current directory
        final String pwd = System.getProperty("user.dir");
        System.out.println("current dir = " + pwd);

        // relative path to file
        String fileName = pwd + "\\file1.txt";
        // FileReader read text file, wrapped inside the BufferedReader
        FileReader fr = null;
        try {
            fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                System.out.flush();
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // FileReader reads text file, wrapped inside a BufferedReader

    }
}
